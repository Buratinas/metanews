<?php

declare(strict_types=1);

namespace App\Storage;

class FileStorage
{
    /**
     * @var string
     */
    private string $storageLocation = '';

    public function __construct(string $applicationPath = '', $entityStorage = '/var/storage')
    {
        $this->storageLocation = $applicationPath . $entityStorage;

        if (!is_dir($this->storageLocation)) {
            mkdir($this->storageLocation, 0777, true);
        }
    }

    public function loadFromFile($filename): ?string
    {
        $storage = $this->storageLocation . $filename;
        if (!is_file($storage)) {
            return null;
        }

        return file_get_contents($storage);
    }

    public function saveToFile($filename, $data): bool
    {
        return (bool) file_put_contents($this->storageLocation . $filename, $data);
    }
}
