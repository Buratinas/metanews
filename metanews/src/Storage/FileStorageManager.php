<?php

declare(strict_types=1);

namespace App\Storage;

use App\Entity\EntityInterface;
use Symfony\Component\Serializer\SerializerInterface;

class FileStorageManager implements ManagerInterface
{
    /**
     * @var string|null
     */
    private ?string $registeredEntity = null;

    private ?string $repositoryClass = null;

    private ?array $loadedObjects = null;

    /**
     * @var FileStorage
     */
    private FileStorage $fileStorage;

    /**
     * @var SerializerInterface
     */
    private SerializerInterface $serializer;

    public function __construct(FileStorage $fileStorage, SerializerInterface $serializer)
    {
        $this->fileStorage = $fileStorage;
        $this->serializer = $serializer;
    }

    public function save(array $objects): bool
    {
        $this->updateObjects($objects);

        return $this->flush();
    }

    public function load(): array
    {
        if (!$this->loadedObjects) {
            $serializedObjects = $this->fileStorage->loadFromFile($this->registeredEntity);
            if (!$serializedObjects) {
                $this->loadedObjects = [];
                
                return $this->loadedObjects;
            }

            $deserializeData = $this->serializer->deserialize($serializedObjects, sprintf('%s[]', $this->repositoryClass), 'json');
            $this->updateObjects($deserializeData);
        }


        return $this->loadedObjects ?? [];
    }

    public function find($id): ?EntityInterface
    {
        $this->load();

        return $this->loadedObjects[$id] ?? null;
    }

    public function insert(EntityInterface $object)
    {
        $this->load();
        $this->loadedObjects[$object->getId()] = $object;
        $this->flush();

        return $object->getId();
    }

    public function flush(): bool
    {
        if (null !== $this->loadedObjects) {
            $objects = $this->serializer->serialize($this->loadedObjects, 'json');

            return $this->fileStorage->saveToFile($this->registeredEntity, $objects);
        }

        return false;
    }

    public function getLastInsertedId(): int
    {
        $this->load();

        return empty($this->loadedObjects) ? 0 : max(array_keys($this->loadedObjects));
    }

    public function registerEntityRepository(string $repositoryClass, string $repositoryName): void
    {
        $this->registeredEntity = $repositoryName;
        $this->repositoryClass = $repositoryClass;
    }

    public function remove(EntityInterface $object): bool
    {
        $this->load();

        if (!$this->loadedObjects[$object->getId()]) {
            return false;
        }

        unset($this->loadedObjects[$object->getId()]);

        return $this->flush();
    }

    private function updateObjects(array $objects)
    {
        /** @var EntityInterface $object */
        foreach ($objects as $object) {
            $this->loadedObjects[$object->getId()] = $object;
        }
    }
}
