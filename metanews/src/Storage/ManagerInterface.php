<?php

declare(strict_types=1);

namespace App\Storage;

use App\Entity\EntityInterface;

interface ManagerInterface
{
    public function save(array $objects): bool;

    public function load(): array;

    public function find($id): ?EntityInterface;

    public function insert(EntityInterface $object);

    public function getLastInsertedId(): int;

    public function flush(): bool;

    public function registerEntityRepository(string $repositoryClass, string $repositoryName): void;

    public function remove(EntityInterface $object): bool;
}
