<?php

declare(strict_types=1);

namespace App\Controller\Frontend;

use App\Repository\SubscriptionRepositoryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FrontendController extends AbstractController
{
    public function index(SubscriptionRepositoryInterface $subscriptionRepository)
    {
        $subscriptions = $subscriptionRepository->findAll();

        return $this->render('index/index.html.twig', [
            'subscriptions' => $subscriptions
        ]);
    }
}
