<?php

declare(strict_types=1);

namespace App\Controller\Api;

use App\Entity\Subscription;
use App\Repository\SubscriptionRepositoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class SubscriptionController
{
    /**
     * @var SubscriptionRepositoryInterface
     */
    private SubscriptionRepositoryInterface $subscriptionRepository;

    /**
     * @var NormalizerInterface
     */
    private NormalizerInterface $normalizer;

    /**
     * @var SerializerInterface
     */
    private SerializerInterface $serializer;

    /**
     * @var ValidatorInterface
     */
    private ValidatorInterface $validator;

    public function __construct(
        SubscriptionRepositoryInterface $subscriptionRepository,
        NormalizerInterface $normalizer,
        SerializerInterface $serializer,
        ValidatorInterface $validator
    ) {
        $this->subscriptionRepository = $subscriptionRepository;
        $this->normalizer = $normalizer;
        $this->serializer = $serializer;
        $this->validator = $validator;
    }

    public function listSubscriptions(): JsonResponse
    {
        $subscriptions = $this->subscriptionRepository->findAll();

        $response = [];
        array_walk($subscriptions, function (Subscription $subscription) use (&$response) {
            $response['subscriptions'][] = $this->normalizer->normalize($subscription, 'json');
        });

        return new JsonResponse($response);
    }

    public function getSubscription(int $id): JsonResponse
    {
        $subscription = $this->subscriptionRepository->find($id);

        if (!$subscription) {
            return new JsonResponse(['status' => 'error', 'errors' => 'subscription not found'], Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse($this->normalizer->normalize($subscription, 'json'));
    }

    public function editSubscription(Request $request, int $id)
    {
        /** @var Subscription $currentSubscription */
        $currentSubscription = $this->subscriptionRepository->find($id);
        if (!$currentSubscription) {
            return new JsonResponse(['status' => 'error'], Response::HTTP_NOT_FOUND);
        }


        /** @var Subscription $subscription */
        $subscription = $this->serializer->deserialize($request->getContent(), Subscription::class, 'json');
        $validationErrors = $this->validator->validate($subscription);
        if (count($validationErrors) > 0) {
            return new JsonResponse([
                'status' => 'error',
                'errors' => (string) $validationErrors
            ]);
        }

        if (!$subscription->getId()) {
            $subscription->setId($currentSubscription->getId());
            $subscription->setCreatedAt($currentSubscription->getCreatedAt());
        }

        if ($this->subscriptionRepository->persist($subscription)) {
            return new JsonResponse(['status' => 'OK'], Response::HTTP_ACCEPTED);
        }

        return new JsonResponse(['status' => 'error'], Response::HTTP_BAD_REQUEST);
    }

    public function deleteSubscription(int $id): JsonResponse
    {
        $subscription = $this->subscriptionRepository->find($id);

        if (!$subscription) {
            return new JsonResponse(['status' => 'error', 'errors' => 'subscription not found'], Response::HTTP_BAD_REQUEST);
        }

        if ($this->subscriptionRepository->remove($subscription)) {
            return new JsonResponse(['status' => 'OK']);
        }

        return new JsonResponse(['status' => 'error', 'errors' => 'could not delete subscription'], Response::HTTP_BAD_REQUEST);
    }

    public function addSubscription(Request $request): JsonResponse
    {
        /** @var Subscription $subscription */
        $subscription = $this->serializer->deserialize($request->getContent(), Subscription::class, 'json');
        $validationErrors = $this->validator->validate($subscription);
        if (count($validationErrors) > 0) {
            return new JsonResponse([
                'status' => 'error',
                'errors' => (string) $validationErrors
            ], Response::HTTP_BAD_REQUEST);
        }
        $insertedId = $this->subscriptionRepository->persist($subscription);

        return new JsonResponse([
            'status' => 'OK',
            'id' => $insertedId
        ], Response::HTTP_CREATED);
    }
}
