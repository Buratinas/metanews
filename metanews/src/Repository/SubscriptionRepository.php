<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Subscription;
use DateTime;

class SubscriptionRepository extends Repository implements SubscriptionRepositoryInterface
{
    public function findAll(): array
    {
        return $this->manager->load();
    }

    public function find(int $id): ?Subscription
    {
        $object = $this->manager->find($id);

        return $object instanceof Subscription ? $object : null;
    }

    public function persist(object $object)
    {
        if ($object instanceof Subscription && !$object->getId()) {
            $object->setId($this->manager->getLastInsertedId() + 1);
        }
        $object->setCreatedAt((new DateTime())->getTimestamp());

        return parent::persist($object);
    }

    public function remove(object $object)
    {
        if ($object instanceof Subscription) {
            return $this->manager->remove($object);
        }

        return false;
    }

    protected function getRepositoryName(): string
    {
        return 'subscription';
    }

    protected function getRepositoryClass(): string
    {
        return Subscription::class;
    }
}
