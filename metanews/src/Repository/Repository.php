<?php

declare(strict_types=1);

namespace App\Repository;

use App\Storage\ManagerInterface;

abstract class Repository
{
    /**
     * @var ManagerInterface
     */
    protected ManagerInterface $manager;

    public function __construct(ManagerInterface $manager)
    {
        $this->manager = $manager;
        $this->manager->registerEntityRepository($this->getRepositoryClass(), $this->getRepositoryName());
    }

    public function persist(object $object)
    {
        return $this->manager->insert($object);
    }

    public function remove(object $object)
    {
        return $this->manager->remove($object);
    }

    public function saveAll(array $objects): bool
    {
        return $this->manager->save($objects);
    }

    abstract protected function getRepositoryName(): string;

    abstract protected function getRepositoryClass(): string;
}
