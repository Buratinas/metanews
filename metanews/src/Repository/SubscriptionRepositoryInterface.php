<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Subscription;

interface SubscriptionRepositoryInterface
{
    public function findAll();

    public function find(int $id);

    public function persist(Subscription $subscription);

    public function remove(Subscription $subscription);
}
