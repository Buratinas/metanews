<?php

declare(strict_types=1);

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Subscription implements EntityInterface
{
    /**
     * @var int
     */
    private ?int $id;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private ?string $name;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private ?string $email;

    /**
     * @var array
     * @Assert\NotBlank()
     */
    private array $categories = [];

    /**
     * @var int
     */
    private ?int $createdAt;

    public function __construct()
    {
        $this->id = null;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Subscription
     */
    public function setId(int $id): ?Subscription
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Subscription
     */
    public function setName(string $name): ?Subscription
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Subscription
     */
    public function setEmail(string $email): ?Subscription
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return array
     */
    public function getCategories(): array
    {
        return $this->categories;
    }

    /**
     * @param array $categories
     * @return Subscription
     */
    public function setCategories(array $categories): Subscription
    {
        $this->categories = $categories;

        return $this;
    }

    /**
     * @return int
     */
    public function getCreatedAt(): int
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     * @return Subscription
     */
    public function setCreatedAt(int $createdAt): Subscription
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
