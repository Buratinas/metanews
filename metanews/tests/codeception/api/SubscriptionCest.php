<?php namespace App\Tests;

use Codeception\Util\HttpCode;

class SubscriptionCest
{
    const API_SUBSCRIPTION = 'api/subscription';

    public function _before(ApiTester $I)
    {
        $I->wantTo('clean up existing stored subscriptions');
        $I->haveHttpHeader('Authorization', 'Basic c2VydmljZTphZG1pbg==');
    }

    private $subscriptions = [
        'john.oliver' => [
            'name' => 'John',
            'email' => 'john@oliver.com',
            'categories' => ['comedy', 'news']
        ],
        'john.cena' => [
            'name' => 'John',
            'email' => 'john@cena.com',
            'categories' => ['sports']
        ],
        'wrong.email' => [
            'name' => 'Wrong',
            'email' => 'wrongemail',
            'categories' => ['errors']
        ],
        'wrong.name' => [
            'name' => '',
            'email' => 'wrong@email.com',
            'categories' => ['errors']
        ],
        'empty.category' => [
            'name' => 'Wrong',
            'email' => 'wrong@email.com',
            'categories' => []
        ]
    ];

    public function addSubscription(ApiTester $I)
    {
        $I->wantTo('add subscription');

        $I->sendPOST(self::API_SUBSCRIPTION, json_encode($this->subscriptions['john.oliver']));

        $I->seeResponseCodeIs(HttpCode::CREATED);
        $I->memorizeFromResponseByJsonPath('subscription.id', 'id');

        $I->seeResponseMatchesJsonType(['status' => 'string', 'id' => 'integer']);
    }

    public function addInvalidEmailSubscription(ApiTester $I)
    {
        $I->wantTo('add invalid subscription');

        $I->sendPOST(self::API_SUBSCRIPTION, json_encode($this->subscriptions['wrong.email']));

        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);

        $I->seeResponseContains('Object(App\\\\Entity\\\\Subscription).email');
        $I->seeResponseContains('This value is not a valid email address.');

        $I->seeResponseMatchesJsonType(['status' => 'string', 'errors' => 'string']);
    }

    public function addInvalidNameSubscription(ApiTester $I)
    {
        $I->wantTo('add invalid subscription');

        $I->sendPOST(self::API_SUBSCRIPTION, json_encode($this->subscriptions['wrong.name']));

        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);

        $I->seeResponseContains('Object(App\\\\Entity\\\\Subscription).name');
        $I->seeResponseContains('This value should not be blank.');

        $I->seeResponseMatchesJsonType(['status' => 'string', 'errors' => 'string']);
    }

    public function addInvalidCategorySubscription(ApiTester $I)
    {
        $I->wantTo('add invalid subscription');

        $I->sendPOST(self::API_SUBSCRIPTION, json_encode($this->subscriptions['empty.category']));

        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);

        $I->seeResponseContains('Object(App\\\\Entity\\\\Subscription).categories');
        $I->seeResponseContains('This value should not be blank.');

        $I->seeResponseMatchesJsonType(['status' => 'string', 'errors' => 'string']);
    }

    public function getSubscriptions(ApiTester $I)
    {
        $I->wantTo('get all subscriptions');

        $I->sendGET(self::API_SUBSCRIPTION);

        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseMatchesJsonType([
            'subscriptions' => [
                [
                    'id' => 'integer',
                    'name' => 'string',
                    'email' => 'email|string',
                    'categories' => 'array'
                ]
            ]
        ]);
    }

    public function getSingleSubscription(ApiTester $I)
    {
        $I->wantTo('get all subscription by id');

        $id = $I->mustRemember('subscription.id');

        $I->sendGET(sprintf('%s/%s', self::API_SUBSCRIPTION, $id));

        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseMatchesJsonType([
            'id' => 'integer',
            'name' => 'string',
            'email' => 'email|string',
            'categories' => 'array'
        ]);

        $I->seeResponseContainsJson($this->subscriptions['john.oliver']);
    }

    public function updateSubscription(ApiTester $I)
    {
        $I->wantTo('edit existing subscription');
        $id = $I->mustRemember('subscription.id');

        $I->sendPUT(sprintf('%s/%s', self::API_SUBSCRIPTION, $id), json_encode($this->subscriptions['john.cena']));
        $I->seeResponseContainsJson(['status' => 'OK']);

        $I->sendGET(sprintf('%s/%s', self::API_SUBSCRIPTION, $id));
        $I->seeResponseContainsJson($this->subscriptions['john.cena']);
    }

    public function removeSubscription(ApiTester $I)
    {
        $I->wantTo('delete existing subscription');
        $id = $I->mustRemember('subscription.id');

        $I->sendDELETE(sprintf('%s/%s', self::API_SUBSCRIPTION, $id));
        $I->seeResponseContainsJson(['status' => 'OK']);

        $I->sendGET(sprintf('%s/%s', self::API_SUBSCRIPTION, $id));

        $I->seeResponseCodeIs(HttpCode::NOT_FOUND);
        $I->seeResponseContainsJson(['status' => 'error', 'errors' => 'subscription not found']);
    }
}
