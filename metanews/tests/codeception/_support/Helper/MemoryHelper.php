<?php

declare(strict_types=1);

namespace App\Tests\Helper;

trait MemoryHelper
{
    private static $memory = [];

    public function memorize($key, $value): void
    {
        $this->amGoingTo(sprintf('memorize value %s => %s', $key, $value));
        self::$memory[$key] = $value;
    }

    public function tryToRemember(string $key)
    {
        return self::$memory[$key] ?? null;
    }

    public function mustRemember(string $key)
    {
        if (empty(self::$memory[$key])) {
            $message = sprintf('I cannot remember vital information: %s', $key);

            throw new \InvalidArgumentException($message);
        }

        return self::$memory[$key];
    }


    public function memorizeFromResponseByJsonPath(string $key, string $path): void
    {
        $value = current($this->grabDataFromResponseByJsonPath($path));
        $this->memorize($key, $value);
    }
}
