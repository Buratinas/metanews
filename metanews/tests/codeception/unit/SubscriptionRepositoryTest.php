<?php

namespace App\Tests\Unit;

use App\Entity\Subscription;
use App\Repository\SubscriptionRepository;
use DateTime;
use PHPUnit\Framework\TestCase;

class SubscriptionRepositoryTest extends TestCase
{
    use ManagerHelper;

    /**
     * @var SubscriptionRepository
     */
    private SubscriptionRepository $repository;

    public function setUp(): void
    {
        $this->contents = [
            1 => $this->createSubscription(1, 'Jim', 'jim@beam.com', ['drinks'], 123),
            2 => $this->createSubscription(2, 'Tom', 'tom@jerry.com', ['animation'], 234),
        ];

        $this->repository = new SubscriptionRepository($this->createManager());
    }

    public function testFindAll(): void
    {
        $subscriptions = $this->repository->findAll();

        $this->assertCount(2, $subscriptions);

        foreach ($subscriptions as $subscription) {
            $this->assertInstanceOf(Subscription::class, $subscription);
        }
    }

    public function testFind(): void
    {
        $subscription = $this->repository->find(1);

        $this->assertInstanceOf(Subscription::class, $subscription);

        $this->assertEquals(1, $subscription->getId());
        $this->assertEquals('Jim', $subscription->getName());
        $this->assertEquals('jim@beam.com', $subscription->getEmail());
        $this->assertEquals(['drinks'], $subscription->getCategories());
        $this->assertEquals(123, $subscription->getCreatedAt());
    }

    public function testFindNonExistingSubscription()
    {
        $this->assertNull($this->repository->find(1234));
    }

    private function createSubscription(int $id, string $name, string $email, array $categories, int $createdAt = null): Subscription
    {
        return (new Subscription())
            ->setId($id)
            ->setName($name)
            ->setCategories($categories)
            ->setEmail($email)
            ->setCreatedAt($createdAt ?? (new DateTime())->getTimestamp());
    }
}
