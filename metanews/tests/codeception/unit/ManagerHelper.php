<?php

namespace App\Tests\Unit;

use App\Storage\ManagerInterface;
use Mockery;

trait ManagerHelper
{
    private array $contents = [];

    private bool $result = true;

    /**
     * @return ManagerInterface|Mockery\LegacyMockInterface|Mockery\MockInterface
     */
    private function createManager()
    {
        $manager = Mockery::mock(ManagerInterface::class);
        $manager->shouldReceive('find')->andReturnUsing($this->findResultClosure());
        $manager->shouldReceive('load')->andReturnUsing($this->loadResultsClosure());
        $manager->shouldReceive('insert')->andReturnUsing($this->insertResultClosure());
        $manager->shouldReceive('save')->andReturnUsing($this->saveResultsClosure());
        $manager->shouldReceive('registerEntityRepository');

        return $manager;
    }

    private function findResultClosure()
    {
        return function ($id) {
            return $this->contents[$id] ?? null;
        };
    }

    private function loadResultsClosure()
    {
        return function () {
            return $this->contents;
        };
    }

    private function saveResultsClosure()
    {
        return function () {
            return $this->result;
        };
    }

    private function insertResultClosure()
    {
        return function () {
            return $this->result;
        };
    }
}
