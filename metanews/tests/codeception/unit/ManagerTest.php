<?php

declare(strict_types=1);

namespace App\Tests\Unit;

use App\Entity\Subscription;
use App\Storage\FileStorage;
use App\Storage\FileStorageManager;
use App\Storage\ManagerInterface;
use DateTime;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

class ManagerTest extends TestCase
{
    /**
     * @var ManagerInterface
     */
    private ManagerInterface $manager;

    /**
     * @var array
     */
    private array $subscriptions = [];

    public function setUp(): void
    {
        $this->manager = $this->createManager(Subscription::class);
        $today = (new DateTime())->getTimestamp();

        $this->subscriptions = [
            1 => $this->createSubscription(1, 'Vin', 'vin@diesel.com', ['movies'], $today),
            2 => $this->createSubscription(2, 'Elon', 'elon@tesla.com', ['cars'], $today),
        ];

        $this->manager->save($this->subscriptions);

    }

    public function testManagerSaveAndLoad()
    {
        $loadedSubscriptions = $this->manager->load();

        $this->assertEquals($this->subscriptions, $loadedSubscriptions);
    }

    public function testManagerFind()
    {
        /** @var Subscription $subscription */
        $subscription = $this->manager->find(1);

        $this->assertInstanceOf(Subscription::class, $subscription);
        $this->assertEquals('Vin', $subscription->getName());
        $this->assertEquals('vin@diesel.com', $subscription->getEmail());
        $this->assertEquals(['movies'], $subscription->getCategories());
    }

    public function testManagerInsert()
    {
        $subscription = $this->createSubscription(3, 'john', 'john@travolta.com', ['movies', 'dancing'], 3456789);

        $this->assertTrue((bool) $this->manager->insert($subscription));

        $subscriptions = $this->manager->load();

        $this->assertCount(3, $subscriptions);
    }

    public function testDifferentManagerCanLoadInsertedDocuments()
    {
        $subscription = $this->createSubscription(3, 'john', 'john@travolta.com', ['movies', 'dancing'], 3456789);

        $this->manager->insert($subscription);
        
        $newManager = $this->createManager(Subscription::class);

        $this->assertInstanceOf(Subscription::class, $newManager->find(3));
    }

    public function testSaveCombinesExistingItemsWithNewObjects()
    {
        $subscriptions = [
            $this->createSubscription(2, 'Jim', 'jim@carrey.com', ['movies'], 3456789),
            $this->createSubscription(3, 'John', 'john@travolta.com', ['movies', 'dancing'], 1234569),
        ];

        $this->manager->save($subscriptions);
        $subscriptions = $this->manager->load();

        $this->assertCount(3, $subscriptions);
        /** @var Subscription $modifiedSubscription */
        $modifiedSubscription = $this->manager->find(2);
        /** @var Subscription $insertedSubscription */
        $insertedSubscription = $this->manager->find(3);

        $this->assertEquals('Jim', $modifiedSubscription->getName());
        $this->assertEquals(3456789, $modifiedSubscription->getCreatedAt());
        $this->assertEquals('John', $insertedSubscription->getName());
        $this->assertEquals(1234569, $insertedSubscription->getCreatedAt());
    }

    private function createSubscription(int $id, string $name, string $email, array $categories, int $createdAt = null): Subscription
    {
        return (new Subscription())
            ->setId($id)
            ->setName($name)
            ->setCategories($categories)
            ->setEmail($email)
            ->setCreatedAt($createdAt ?? (new DateTime())->getTimestamp());
    }

    private function createManager(string $class, string $repositoryName = 'test'): ManagerInterface
    {
        $manager = new FileStorageManager(new FileStorage(getcwd()), $this->getSerializer());
        $manager->registerEntityRepository(Subscription::class, 'test');

        return $manager;
    }


    private function getSerializer(): SerializerInterface
    {
        $encoders = [new JsonEncoder()];

        $normalizers = [
            new ArrayDenormalizer(),
            new GetSetMethodNormalizer(),
        ];

        return new Serializer($normalizers, $encoders);
    }
}
