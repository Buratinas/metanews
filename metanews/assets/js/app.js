/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import '../css/app.css';

require('bootstrap');
require('bootstrap-table');
require('@dashboardcode/bsmultiselect/dist/js/BsMultiSelect.js');
require('@dashboardcode/bsmultiselect/dist/js/BsMultiSelect.esm.min.js');
require('@fortawesome/fontawesome-free/css/all.min.css');
require('@fortawesome/fontawesome-free/js/all.js');

// Need jQuery? Install it with "yarn add jquery", then uncomment to import it.
import $ from 'jquery';
import moment from "moment";

const defaultCategories = [
    {text:"Movies", value:"movies",hidden:false,disabled:false,selected:false},
    {text:"Music",value:"music",hidden:false,disabled:false,selected:false},
    {text:"Comedy",value:"comedy",hidden:false,disabled:false,selected:false},
    {text:"Sports",value:"sports",hidden:false,disabled:false,selected:false},
    {text:"News",value:"news",hidden:false,disabled:false,selected:false}
];

const app = {

    init: function () {
        this.initBootstrapTable();
    },
    initBootstrapTable: function () {
        let $table = $('#subscriptions');
        let $submitButton = $('form button[type=submit]');

        $('form').off().on('submit', e => {
            e.preventDefault();
            let name=$('form input#name').val();
            let email=$('form input#email').val();
            let categories=$('form .badge').map(function() {
                return this.firstElementChild.innerText;
            })
                .get();

            let data = {
                name: name,
                email: email,
                categories: categories
            };

            if ($submitButton.html() === 'Submit') {
                this.addSubscription(data);
            } else {
                this.editSubscription(data);
            }
        });
        $table.on('click', '.remove', e => {
            const id = e.currentTarget.parentElement.parentElement.dataset.uniqueid;
            this.deleteSubscription(id);
        });

        $table.on('click', '.edit', e => {
            const id = e.currentTarget.parentElement.parentElement.dataset.uniqueid;
            const data = $table.bootstrapTable('getRowByUniqueId', id);

            const realData = {
                id: data.id,
                name: data.name,
                email: data.email,
                categories: data.categories.split(),
                createdAt: data.createdAt
            };

            this.addEditInformation(id, realData);
        });

        $('#categorySelect').bsMultiSelect(
            {
                options : this.categorySelection(),
            }
        );
    },
    categorySelection: function (categories) {
        if (categories !== undefined) {
            return defaultCategories.map(function (category) {
                return {text: category.text, value: category.value, hidden: category.hidden, disabled: category.disabled, selected: categories.indexOf(category.text) === 0}
            });
        }

        return defaultCategories;
    },
    deleteSubscription: function (id)
    {
        let $table = $('#subscriptions');

        const action=$table.data('action');
        const location=window.location.origin + action;

        let xhr = new XMLHttpRequest();
        $.ajax(location + "/" + id, {
            type: 'DELETE',
            success: function (data, status, xhr) {
                $table.bootstrapTable('removeByUniqueId', id);
            }
        });
    },
    addEditInformation: function(id, data) {
        let selectedCategories = this.categorySelection(data.categories);
        let $submitButton = $('form button[type=submit]');

        $('#categorySelect').bsMultiSelect(
            {
                options : selectedCategories,
            }
        );
        let name=$('form input#name');
        let email=$('form input#email');
        name.prop('value', data.name);
        email.prop('value', data.email);

        $submitButton.html('Update');
        $submitButton.data('id', id);
    },
    editSubscription: function (subscription) {
        let $submitButton = $('form button[type=submit]');
        let id = $submitButton.data('id');
        let $table = $('#subscriptions');
        let json = JSON.stringify(subscription);

        const action=$table.data('action');
        const location=window.location.origin + action;

        $.ajax(location + '/' + id, {
            type: 'PUT',
            data: json,
            success: function (data, status, xhr) {
                if (data.status === 'OK') {
                    subscription.operate = '<a class="edit" href="javascript:void(0)" title="Remove"><i class="fa fa-edit"></i></a>\n' +
                        '                    <a class="remove" href="javascript:void(0)" title="Remove"><i class="fa fa-trash"></i></a>';
                    $table.bootstrapTable('updateRow', {index: id, row: {
                            id: subscription.id,
                            name: subscription.name,
                            email: subscription.email,
                            categories: subscription.categories.join(', ')
                        }});
                    $submitButton.removeData('id');
                    $submitButton.html('Submit');
                }
            }
        });
    },
    addSubscription: function (subscription) {
        let $table = $('#subscriptions');
        let json = JSON.stringify(subscription);

        const action=$table.data('action');
        const location=window.location.origin + action;

        $.ajax(location, {
            type: 'POST',
            data: json,
            success: function (data, status, xhr) {
                if (data.status === 'OK') {
                    subscription.id = JSON.parse(xhr.responseText).id;
                    subscription.createdAt = moment().format('MMMM DD, YYYY HH:mm');
                    subscription.operate = '<a class="edit" href="javascript:void(0)" title="Remove"><i class="fa fa-edit"></i></a>\n' +
                        '                    <a class="remove" href="javascript:void(0)" title="Remove"><i class="fa fa-trash"></i></a>';
                    $table.bootstrapTable('append', subscription);
                }
            }
        });
    }
};
app.init();
