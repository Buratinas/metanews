# Category list

Implementation checklist:
- setup basic environment (php, symfony, docker, CI)
- add test framework
- implement file repository
- add API for category list
- add deployment setup
- add simple security for authentication
- add frontend controller for display

# Post implementation notes

The task is simple, but made overcomplicated with file storage requirement.  
This makes the task much more of a chore than it actually is. Where you can simply use Doctrine to get it done in several minutes,
implementing file reading/writing repository in file system wastes much time and can never compete with a decent existing solution.  
Neither in terms of speed, nor quality. If the task focused only on this file repository, it might be slightly more interesting.  
However, the task requires to do pretty much everything from backend/frontend to cloud deployment.  
While it's not hard to do any of those parts, when you have to rush through the implementation to get everything done in few hours makes
half baked solution at best.  

I stopped at frontend jquery implementation, as I ran out of time I can dedicate during the weekend for this task.  
This is not a few hours work, as it was advertised, not if you want to do everything well. Here's a simple estimation of how long it takes to do each part:
- Basic application setup (Symfony, docker, CI, test framework integration) - 1-3 hours
- File Repository implementation - 2-4 hours (depending how well it is tested, and how thorough the implementation is)
- API implementation - 1-3 hours (depends how well tested and how thorough it is)
- Security implementation - 1-2 hours
- Basic Frontend implementation (controller with bootstrap and simple styling) - 1-3 hours
- Advanced Frontend implementation (jquery form submissions, live reloading/editing of the list) - 2-4 hours 
- Cloud deployment - 1-3 hours

In total it takes 12-22 hours to do this task while rushing through each part. 
To do each part well with edge cases and proper implementation it can take twice as much.
To compare, doing similar task without file storage requirement and using API platform can cut development time in half.

#### Implemented parts

Parts that were implemented, and their description:
- docker containerization with docker-compose. Frontend build separated into encore container, which deals with frontend dependencies.
- PHP 7.4 + Symfony 5 backend, Codeception for testing (Unit + API).
- Added packages - security (auth), validator (entity validation), serializer (entity json serialization)
- Frontend related packages - twig, webpack-encore-bundle, asset
- `SubscriptionController` - API controller for subscriptions with RESTful API.
- `FrontendController` - subscription list which calls REST endpoints to do CRUD operations without reloading the page.  
- `FileStorageManager` - simulated manager for repository to allow saving/loading entities. Somewhat similar approach to Doctrine manager, which should make it easy to replace.
- `SubscriptionRepository` - Repository for subscriptions, using file storage manager. It takes manager dependency as a constructor argument, and should be easy to replace.
- Frontend used dependencies - `bootstrap `4 for main styling, `bootstrap-table` for table with sorting and some events for remove/edit. `@dashboardcode/bsmultiselect` for multi select list for categories.
- Frontend contains table which is sortable, has pagination and makes asynchronous requests for editing subscriptions.

#### Currently known issues

Due to rushed implementation, several places lack polish or are broken:
- frontend ajax edit works, but does not update edited value properly after ajax request
- categories being edited are not properly updated in select list
- file manager is simplified. It does all the necessary CRUD operations, but is not thoroughly tested.
- API is implemented to work with existing SubscriptionRepository and does validation/serialization inside. This can be done in a cleaner manner.
- Basic auth is chosen to do quick authentication as there is no decent storage mechanism, and time is the biggest constraint.
- Missing a lot of tests, but API is covered with minimal amount of tests and several key areas are tested with unit tests.
- cloud deployment was not finished due to lack of time.

### Setup

***Requirements***

- docker
- docker-compose

If you have composer in your machine, you can install dependencies:
```bash
composer install --ignore-platform-reqs --no-scripts -n
```

To setup, checkout the repository and run:

```bash
docker-compose up -d
```

If you had no composer in your machine, run
```bash
docker-compose exec -T metasite-app composer install
```

Run tests:

```bash
docker-compose exec -T metasite-app vendor/bin/codecept run --steps
```

Update hosts file with:
`127.0.0.1 metasite.local`

Frontend endpoint:
- `http://metasite.local/index`
Backend API endpoint:
- `http://metasite.local/v1/api/subscription`
Supported methods ['GET', 'POST', 'PUT', 'DELETE']  
Accessing endpoints are done using basic auth with these credentials `service:admin`
