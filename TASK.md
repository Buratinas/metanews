Suprogramuoti naujienų prenumeratos anketą:
1. Reikalavimai anketai  
1.1. Vartotojas anketoje nurodo savo vardą, el. pašto adresą ir pasirenka jį dominančias
kategorijas. Anketos HTML maketas pasirenkamas savo nuožiūra.  
1.2. Visi laukai privalomi (t. y. nurodyti vardą, el. paštą ir pasirinkti bent vieną kategoriją
vartotojas privalo).  
1.3. Naujienų kategorijų sąrašas yra fiksuotas, tačiau ateityje gali būti plečiamas, todėl reikia
tai numatyti programuojant.  
1.4. Duomenys turi būti išsaugomi faile, o ne duomenų bazėje - būtinas reikalavimas.  
2. Reikalavimai prenumeratorių sąrašo peržiūrai  
2.1.Sąrašas apsaugotas prisijungimu (vartotojo vardas ir slaptažodis).  
2.2.Sąraše rodomi prenumeratorių vardai, el. pašto adresai ir pasirinktos naujienų kategorijos.  
2.3.Sąrašą galima išrikiuoti pagal registracijos datą, el. pašto adresą, vardą.  
2.4.Turi būti galimybė redaguoti pasirinkto prenumeratoriaus vardą ir el. pašto adresą, tačiau
neturi būti leidžiama juos „sugadinti“ (t. y. nepriimti tuščių laukų ir el. pašto adresas turi
atrodyti teisingas).  
2.5.Galimybė ištrinti prenumeratorių iš sąrašo.  
3. Papildomi reikalavimai programiniam kodui  
3.1.PHP versija >= 7.0  
3.2.Užduotį būtina atlikti su Symfony 4 framework‘u.  
Papildomi balai bus skiriami, jei:  
- Užduotis bus atlikta ne tik „sausai“ išpildant reikalavimus, bet pritaikant visas turimas
„usability“ ir programavimo (HTML, CSS, Javascript) žinias. Kitaip tariant, reikėtų
padaryti daugiau, nei reikalaujama.
- Bus numatyta, kad kodas ateityje bus tobulinamas ir pvz. naudojamas kituose
komponentuose.
- Bus panaudotas software container sprendimas (pvz. Docker).
Reikalavimai pateikiamam rezultatui:
Užduotį prašytume pateikti el. paštu: jobs@metasite.net.


Prašome darbo rezultatą patalpinti prieinamoje vietoje peržiūrai (pvz. www.herokuapp.com,
https://aws.amazon.com/free/ , https://cloud.google.com/free/ ,
https://azure.microsoft.com/en-us/offers/ms-azr-0044p/ ir pan. ), o source code patalpinti
kuriame nors viešajame kodo versijavimo servise (github, bitbucket ar kt.).
